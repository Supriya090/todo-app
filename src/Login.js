import React, { useContext } from "react";
import "./login.css";
import useForm from "./customhooks/useForm";
import validate from "./customhooks/validateInfo";
import { LoginContext } from "./contexts/LoginContext";
import Input from "./components/Input";

function Login(submitForm) {
  const { handleChange, values, handleSubmit, errors } = useForm({
    submitForm,
    validate,
  });

  const { setUsername } = useContext(LoginContext);

  return (
    <>
      <h1>TO-DO LIST</h1>
      <div className='formContainer'>
        <div className='formContent'>
          <form onSubmit={handleSubmit} className='form'>
            <h2>Sign Up</h2>
            <Input
              id='username'
              type='text'
              name='username'
              label='Username'
              value={values.username}
              className='formInput'
              placeholder='Enter your Username'
              onChange={(setUsername(values.username), handleChange)}
              error={errors.username}
            />
            <Input
              id='email'
              type='email'
              name='email'
              label='Email'
              value={values.email}
              className='formInput'
              placeholder='Enter your Email'
              onChange={handleChange}
              error={errors.email}
            />
            <Input
              id='password'
              type='password'
              name='password'
              label='Password'
              value={values.password}
              className='formInput'
              placeholder='Enter your Password'
              onChange={handleChange}
              error={errors.password}
            />
            <Input
              id='password2'
              type='password'
              name='password2'
              label='Confirm Password'
              value={values.password2}
              className='formInput'
              placeholder='Confirm your Password'
              onChange={handleChange}
              error={errors.password2}
            />
            <button className='formInputButton' type='submit'>
              {console.log(Object.keys(errors).length)}
              Sign Up
            </button>
          </form>
        </div>
      </div>
    </>
  );
}

export default Login;
