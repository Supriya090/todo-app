import React from "react";
import "../login.css";

function Input({ value, error, label, id, ...inputProps }) {
  return (
    <div className='formInputs'>
      <label htmlFor={id} className='formLabel'>
        {label}
      </label>
      <input id={id} value={value} {...inputProps} />
      {error && <p>{error}</p>}
    </div>
  );
}

export default Input;
