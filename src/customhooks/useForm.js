import { useState, useEffect, useContext } from "react";
import validate from "./validateInfo";
import { LoginContext } from "../contexts/LoginContext";

const useForm = () => {
  const [values, setValues] = useState({
    username: "",
    email: "",
    password: "",
    password2: "",
  });

  const { setShowTodo } = useContext(LoginContext);

  const [errors, setErrors] = useState({});
  const [isSubmitting, setIsSubmitting] = useState(false);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setValues({
      ...values,
      [name]: value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setErrors(validate(values));
    setIsSubmitting(true);
  };

  useEffect(() => {
    if (Object.keys(errors).length === 0 && isSubmitting) {
      setShowTodo(true);
      console.log("Submitted");
    }
  }, [errors]);

  return { handleChange, values, handleSubmit, errors };
};

export default useForm;
